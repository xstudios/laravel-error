@extends('Xstudios\Laravel\Error::base')

@section('content')

    <h1>401 Not Authorized</h1>
    <div class="alert alert-danger">
        <p>You are not authorized to view this page. <a href="{{ URL::route('home') }}" class="alert-link">Take me home!</a></p>
    </div>

@stop
