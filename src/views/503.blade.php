@extends('Xstudios\Laravel\Error::base')

@section('content')

    <h1>503 Service Unavailable</h1>
    <div class="alert alert-danger">
        <p>We're down for some routine maintenance. <strong>We'll be back shortly!</strong></p>
    </div>

@stop
