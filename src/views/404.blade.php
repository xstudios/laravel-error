@extends('Xstudios\Laravel\Error::base')

@section('content')

    <h1>404 Not Found</h1>
    <div class="alert alert-danger">
        <p>The page you are looking for does not exist. <a href="{{ URL::route('home') }}" class="alert-link">Take me home!</a></p>
    </div>

@stop
