<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ \Config::get('Xstudios\Error::config.app_name') }}</title>

        <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ URL::asset('favicon.ico') }}" type="image/x-icon">

        <link rel="stylesheet" media="all" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    </head>

    <body>

        <div class="container">

            @yield('content')

        </div> <!-- /container -->

        <!-- Placed at the end of the document so the pages load faster -->
        <!-- Grab Google CDN jQuery...fall back to local -->
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script type="text/javascript">!window.jQuery && document.write("<script src=\"{{ URL::asset('js/jquery.js') }}\"><\/script>")</script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    </body>
</html>
