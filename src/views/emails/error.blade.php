<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>

    <strong>DOMAIN:</strong> {{ $domain_name }} &mdash; {{ $domain_ip }}<br/>
    <strong>TIMESTAMP:</strong> {{ $timestamp }}<br/>
    <strong>USER IP:</strong> {{ $ip_address }}<br/>
    <strong>USER AGENT:</strong> {{ $user_agent }}<br/><br/>

    <strong>ERROR:</strong> {{ $error }} at {{ $error_file }} : {{ $error_line }}<br/><br/>

    <strong>STACKTRACE:</strong><br/>
    {{ str_replace('#', '<br/>#', $stacktrace) }}<br/><br/>

    <strong>GET / POST:</strong>
    <pre>{{ print_r($input, 1) }}</pre>

    <strong>SESSION:</strong>
    <pre>{{ print_r($session, 1) }}</pre>

</body>
</html>
