@extends('Xstudios\Laravel\Error::base')

@section('content')

    <h1>500 Error</h1>
    <div class="alert alert-danger">
        <p>Something went terribly wrong.  We are investigating the issue.</p>
    </div>

@stop
