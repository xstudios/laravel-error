<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Exception Email
    |--------------------------------------------------------------------------
    |
    | Exception email settings.
    |
    */

    'app_name'   => 'Laravel Skeleton',
    'to_email'   => 'tsantor@xstudios.agency',
    'from_email' => 'no-reply@laravelskeleton.dev',

    /*
    |--------------------------------------------------------------------------
    | 401 Unauthorized
    |--------------------------------------------------------------------------
    |
    | View to render when encountering a 401 Unauthorized error.
    |
    */

    '401_error' => 'Xstudios\Laravel\Error::401',

    /*
    |--------------------------------------------------------------------------
    | 403 Forbidden
    |--------------------------------------------------------------------------
    |
    | View to render when encountering a 403 Forbidden error.
    |
    */

    '403_error' => 'Xstudios\Laravel\Error::403',

    /*
    |--------------------------------------------------------------------------
    | 404 Not Found
    |--------------------------------------------------------------------------
    |
    | View to render when encountering a 404 Not Found error.
    |
    */

    '404_error' => 'Xstudios\Laravel\Error::404',

    /*
    |--------------------------------------------------------------------------
    | 500 Internal Server Error
    |--------------------------------------------------------------------------
    |
    | View to render when encountering a 500 Internal Server Error.
    |
    */

    '500_error' => 'Xstudios\Laravel\Error::500',

    /*
    |--------------------------------------------------------------------------
    | 501 Not Implemented
    |--------------------------------------------------------------------------
    |
    | View to render when encountering a 501 Not Implemented error.
    |
    */

    '501_error' => 'Xstudios\Laravel\Error::501',

    /*
    |--------------------------------------------------------------------------
    | 503 Service Unavailable
    |--------------------------------------------------------------------------
    |
    | View to render when encountering a 503 Service Unavailable error.
    |
    */

    '503_error' => 'Xstudios\Laravel\Error::503'

);

