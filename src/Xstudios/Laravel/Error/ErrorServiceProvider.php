<?php namespace Xstudios\Laravel\Error;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Exception;

class ErrorServiceProvider extends ServiceProvider {

    const PACKAGE_ALIAS = 'Xstudios\Laravel\Error';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected $config;

    // ------------------------------------------------------------------------

    protected function getServiceProviderConfig() {
        return Config::get(static::PACKAGE_ALIAS . '::config');
    }

    // ------------------------------------------------------------------------

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('xstudios/laravel-error', static::PACKAGE_ALIAS, __DIR__.'/../../..');

        $this->config = $this->getServiceProviderConfig();

        // Handle exceptions
        $this->handleExceptions();
    }

    // ------------------------------------------------------------------------

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->register503();
    }

    // ------------------------------------------------------------------------

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    // ------------------------------------------------------------------------

    /**
     * Handles any app exceptions/errors.
     *
     * @return Response
     */
    protected function handleExceptions()
    {
        $this->app->error(function(Exception $exception, $code)
        {
            $url  = Request::fullUrl();
            $data = array('code' => $code);

            // Return appropriate error response
            switch ($code) {
                case 401: // 401 Unauthorized
                    Log::warning('401 Unauthorized for URL: ' . $url);
                    $view = $this->config['401_error'];
                    return Response::view($view, $data, $code);

                case 403: // 403 Forbidden
                    Log::warning('403 Forbidden for URL: ' . $url);
                    $view = $this->config['403_error'];
                    return Response::view($view, $data, $code);

                case 404: // 404 Not Found
                    Log::warning('404 Not Found for URL: ' . $url);
                    $view = $this->config['404_error'];
                    return Response::view($view, $data, $code);

                case 501: // 501 Not Implemented
                    Log::warning('501 Not Implemented for URL: ' . $url);
                    $view = $this->config['501_error'];
                    return Response::view($view, $data, $code);

                // Any codes which are not specified
                default: // 500 Internal Server Error
                    Log::error($exception);
                    // Send email on uncaught exception
                    $this->_sendEmail($exception, $code);
                    return;
            }
        });
    }

    // ------------------------------------------------------------------------
    /**
     * Send an email to the developer when an uncaught exception occurs.
     * NOTE: Only sends an email if in a production environment.
     *
     * @param  Exception $exception
     * @param  [type]    $code
     * @return void
     */
    private function _sendEmail(Exception $exception, $code) {
        if ($this->app->environment() == 'production')
        {
            $data = array(
                // General info
                'domain_name' => $_SERVER['SERVER_NAME'],
                'domain_ip'   => isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '?',
                'timestamp'   => date('Y-m-d h:i:s'),
                'ip_address'  => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '?',
                'user_agent'  => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '?',

                // What's the issue and where is it?
                'error'       => $exception->getMessage(),
                'error_file'  => $exception->getFile(),
                'error_line'  => $exception->getLine(),
                'stacktrace'  => $exception->getTraceAsString(),

                // Add GET/POST/SESSION data for help with debugging
                'input'       => Input::all(),
                'session'     => Session::all()
            );

            // Get service provider config
            $config = $this->config;

            Mail::send(static::PACKAGE_ALIAS . '::emails.error', $data, function($message) use ($config)
            {
                $message->from($config['from_email']);
                $message->to($config['to_email']);
                $message->subject($config['app_name'] . ' Error');
            });

            Log::info('Error Email sent to ' . $config['to_email']);
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Display a custom 503 view for our application.
     *
     * @return Response
     */
    protected function register503()
    {
        $this->app->down(function()
        {
            $url  = Request::fullUrl();
            Log::warning('503 Service Unavailable for URL: ' . $url);
            $view = View::make($this->config['503_error']);
            return Response::make($view , 503);
        });
    }

    // ------------------------------------------------------------------------

}
