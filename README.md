# Laravel Error Handler

## Overview
Provides a quick and easy way to have basic error handling up and running for 401, 403, 404, 500, 501 and 503 errors. More importantly, any uncaught exceptions are emailed to you right away in a production environment.

## Installation
Open your `composer.json` file and add the following to the `repositories` array:

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/xstudios/laravel-error.git"
        }
    ],

The add the folllowing to the `require` object:

    "xstudios/laravel-error": "v0.1.0"

Or to stay on the bleeding edge (stability not guaranteed):

    "xstudios/laravel-error": "dev-master"

### Install the dependencies

    php composer install

or

    php composer update

Once the package is installed you need to register the service provider with the application. Open up `app/config/app.php` and find the `providers` key.

    'providers' => array(
        # Other providers here ...
        'Xstudios\Laravel\Error\ErrorServiceProvider',
    )

## Configuration
### Config
To configure the package, you can use the following command to copy the configuration file to `app/config/packages/xstudios/laravel-error`.

    $ php artisan config:publish xstudios/laravel-error

> NOTE: The settings themselves are documented inside `config.php`.

### Views
To override the default package views, you can use the following command to copy the view files to `app/config/packages/xstudios/laravel-error`.

    $ php artisan view:publish xstudios/laravel-error

## Contribute
In lieu of a formal styleguide, take care to maintain the existing coding style.

## License
MIT License (c) [X Studios](http://xstudiosinc.com)
