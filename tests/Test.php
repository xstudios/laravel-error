<?php

class Test extends TestCase {

    public function test404()
    {
        $crawler = $this->client->request('GET', '/invalid-url-test');

        $this->assertTrue($this->client->getResponse()->isOk());

        $this->assertCount(1, $crawler->filter('h1:contains("404 Not Found")'));
    }

}
